### Summary

A brief summary of the bug. If it is an error, please paste it in https://paste.gg/ and attach the link here.

*Make sure that you're using the latest version of the plugin. If you aren't using the latest version of the plugin, you won't receive support.*

### Steps to reproduce

1. Go to...
2. Press...
3. Close...

### Observed result

The observed result when trying to reproduce this bug.

### Expected result

The expected result.

### Server information

- Server software: e.g Paper 1.17.1.
- Plugin version: e.g 1.1.4.
- Plugins (Optional): e.g EssentialsX, LuckPerms, Vault, etc.

### Additional information

Add some additional information, like: the bug didn't happen in version 1.1.3.

/label ~bug
